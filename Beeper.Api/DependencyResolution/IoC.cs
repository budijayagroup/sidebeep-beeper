using StructureMap;
using BeeperServices.Repositories;
using BeeperServices.Services;

namespace BeeperServices.Api {
    public static class IoC {
        private static log4net.ILog _logger = log4net.LogManager.GetLogger("Sidebeep.Beeper");

        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });

                            x.For<IBeeperRepository>().Use<BeeperRepository>();
                            x.For<IBeeperService>().Use<BeeperService>();
                        });
            return ObjectFactory.Container;
        }
    }
}