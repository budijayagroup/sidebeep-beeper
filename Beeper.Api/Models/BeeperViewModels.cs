﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace BeeperServices.Api.Models
{
    public class FindBeeperViewModel
    {
        public string TokenAccess { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(TokenAccess))
            {
                errorMessage = "Token Access cannot empty!";
                return false;
            }

            return true;
        }
    }

    public class GetBeeperViewModel
    {
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }

    public class ManageBeeperViewModel
    {
        public string TokenAccess { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string IdentityNumber { get; set; }
        public string TaxNumber { get; set; }
        public string Description { get; set; }
        public string FaceImage { get; set; }
        public string ReferenceSidebeep { get; set; }
        public string MarketerSidebeep { get; set; }
        public string UniversityName { get; set; }
        public int Status{ get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }
}