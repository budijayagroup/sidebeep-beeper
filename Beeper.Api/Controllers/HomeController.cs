﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BeeperServices.Services;

namespace BeeperServices.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
