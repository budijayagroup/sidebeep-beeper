﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using BeeperServices.Api.Models;
using BeeperServices.Services;
using System;
using Sidebeep.Data;

namespace BeeperServices.Api.Controllers.Api
{
    public class BeeperController : ApiController
    {
        public BeeperService _siderService;

        private string errorMessage = "";

        public BeeperController()
        {
            _siderService = new BeeperService();
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Find(FindBeeperViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _siderService.FindByTokenAccess(model.TokenAccess);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No sider found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetBeeperViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _siderService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Manage(ManageBeeperViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var beeper = _siderService.FindByTokenAccess(model.TokenAccess);

            if (beeper == null)
            {
                var newBeeper = new Beeper();

                newBeeper.TokenAccess = model.TokenAccess;
                newBeeper.CreatedDate = DateTime.Now;
                newBeeper.UpdatedDate = DateTime.Now;
                newBeeper.FirstName = model.FirstName;
                newBeeper.LastName = model.LastName;
                newBeeper.Gender = model.Gender;
                newBeeper.DateOfBirth = model.DateOfBirth;
                newBeeper.PhoneNumber = model.PhoneNumber;
                newBeeper.City = model.City;
                newBeeper.Province = model.Province;
                newBeeper.Country = model.Country;
                newBeeper.IdentityNumber = model.IdentityNumber;
                newBeeper.TaxNumber = model.TaxNumber;
                newBeeper.Description = model.Description;
                newBeeper.FaceImage = model.FaceImage;
                newBeeper.ReferenceSidebeep = model.ReferenceSidebeep;
                newBeeper.MarketerSidebeep = model.MarketerSidebeep;
                newBeeper.UniversityName = model.UniversityName;
                newBeeper.Status = model.Status;

                var result = _siderService.CreateBeeper(newBeeper);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
            else
            {
                beeper.TokenAccess = model.TokenAccess;
                beeper.UpdatedDate = DateTime.Now;
                beeper.FirstName = model.FirstName;
                beeper.LastName = model.LastName;
                beeper.Gender = model.Gender;
                beeper.DateOfBirth = model.DateOfBirth;
                beeper.PhoneNumber = model.PhoneNumber;
                beeper.City = model.City;
                beeper.Province = model.Province;
                beeper.Country = model.Country;
                beeper.IdentityNumber = model.IdentityNumber;
                beeper.TaxNumber = model.TaxNumber;
                beeper.Description = model.Description;
                beeper.FaceImage = model.FaceImage;
                beeper.ReferenceSidebeep = model.ReferenceSidebeep;
                beeper.MarketerSidebeep = model.MarketerSidebeep;
                beeper.UniversityName = model.UniversityName;
                beeper.Status = model.Status;

                var result = _siderService.UpdateBeeper(beeper);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
