﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Sidebeep.Data;
using Sidebeep.ORM;

namespace BeeperServices.Repositories
{
    public interface IBeeperRepository
    {
        Beeper Find(int id);
        Beeper FindByTokenAccess(string tokenAccess);
        List<Beeper> Get();
        Beeper Create(Beeper entity);
        bool Update(Beeper entity);
        bool Delete(Beeper entity);

    }

    public class BeeperRepository : IBeeperRepository
    {
        private DataContext db;

        public BeeperRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Beeper Find(int id)
        {
            var ds = db.Beeper.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public Beeper FindByTokenAccess(string tokenAccess)
        {
            var ds = db.Beeper.FirstOrDefault(x => x.TokenAccess == tokenAccess);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Beeper> Get()
        {
            return db.Beeper.ToList();
        }

        public Beeper Create(Beeper entity)
        {
            entity = db.Beeper.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Beeper entity)
        {
            var ds = db.Beeper.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Beeper entity)
        {
            var ds = db.Beeper.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Beeper.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
