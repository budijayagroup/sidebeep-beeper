﻿using System;
using System.Collections.Generic;
using System.Transactions;
using BeeperServices.Repositories;
using Sidebeep.Data;

namespace BeeperServices.Services
{
    public interface IBeeperService
    {
        Beeper FindByTokenAccess(string tokenAccess);
        List<Beeper> Get();
        Beeper CreateBeeper(Beeper entity);
        bool UpdateBeeper(Beeper entity);
        bool DeleteBeeper(Beeper entity);
    }

    public class BeeperService : IBeeperService
    {
        private BeeperRepository _siderRepository;

        public BeeperService()
        {
            _siderRepository = new BeeperRepository();
        }

        public Beeper FindByTokenAccess(string tokenAccess)
        {
            try
            {
                return _siderRepository.FindByTokenAccess(tokenAccess);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Beeper> Get()
        {
            try
            {
                return _siderRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Beeper>();
            }
        }

        public Beeper CreateBeeper(Beeper entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _siderRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateBeeper(Beeper entity)
        {
            return _siderRepository.Update(entity);
        }

        public bool DeleteBeeper(Beeper entity)
        {
            return _siderRepository.Delete(entity);
        }
    }
}
